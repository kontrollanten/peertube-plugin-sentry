import * as Sentry from '@sentry/browser'
import { captureConsoleIntegration, HttpClient as HttpClientIntegration, ExtraErrorData as ExtraErrorDataIntegration } from '@sentry/integrations';

function register ({ registerHook, peertubeHelpers }) {
  peertubeHelpers.getSettings()
    .then((settings = {}) => {
      return Promise.all([
        Promise.resolve(settings),
        peertubeHelpers.getServerConfig()
      ]);
    })
    .then(([settings, serverConfig = {}]) => {
      Sentry.init({
        dsn: settings.dsn,
        environment: settings.environment,
        ignoreErrors: [
          'Non-Error exception captured'
        ],
        integrations: [
          new Sentry.BrowserTracing(),
          new HttpClientIntegration({ // Only log HTTP status code 500-599
            sendDefaultPii: true, // Send header and cookie data
          }),
          new ExtraErrorDataIntegration(), // extracts all non-native attributes from the error object and attaches them to the event as the extra data
          captureConsoleIntegration(['warn', 'error']),
        ],
        release: settings.release || serverConfig.serverVersion,
        sampleRate: settings.sampleRate,
        tracesSampleRate: settings.tracesSampleRate,
        beforeSend: (event, hint) => {
          const { effectiveType, type, downlink } = navigator.connection || { effectiveType: 'n/a', type: 'n/a', downlink: 'n/a' };

          Sentry.setTag('network_status', navigator.onLine);
          Sentry.setTag('network_effective_type', effectiveType);
          Sentry.setTag('network_type', type);
          Sentry.setTag('network_downlink', downlink);

          return event;
        },
      });

      const logNetworkStatus = () => {
        Sentry.captureMessage("browser is " + navigator.onLine, "info");
      };

      logNetworkStatus();

      window.addEventListener('offline', logNetworkStatus);
      window.addEventListener('online', logNetworkStatus);
    })
    .catch(error => {
      console.error('sentry-plugin failed', { error })
    })
}

export {
  register
}
