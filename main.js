async function register ({
  registerSetting,
}) {
  registerSetting({
    name: 'dsn',
    label: 'Sentry DSN',
    type: 'input',
    descriptionHTML: '<a target="_blank" href="https://docs.sentry.io/product/sentry-basics/dsn-explainer/#where-to-find-your-dsn">Data source name</a>',
    private: false,
  })

  registerSetting({
    name: 'environment',
    label: 'Environment',
    type: 'input',
    descriptionHTML: '<a target="_blank" href="https://docs.sentry.io/platforms/javascript/configuration/options/#environment">Environment</a>',
    private: false,
  })

  registerSetting({
    name: 'sampleRate',
    label: 'Sample rate',
    type: 'input',
    descriptionHTML: '<a target="_blank" href="https://docs.sentry.io/platforms/javascript/configuration/options/#sample-rate">Sample rate</a>',
    private: false,
  })

  registerSetting({
    name: 'tracesSampleRate',
    label: 'Traces sample rate',
    type: 'input',
    descriptionHTML: '<a target="_blank" href="https://docs.sentry.io/platforms/javascript/configuration/options/#traces-sample-rate">Traces sample rate</a>',
    private: false,
  })

  registerSetting({
    name: 'release',
    label: 'Release',
    type: 'input',
    descriptionHTML: '<a target="_blank" href="https://docs.sentry.io/platforms/javascript/configuration/options/#release">Release</a>, defaults to running Peertube version.',
    private: false,
  })
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
